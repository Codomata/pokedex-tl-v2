﻿using System;
using System.Net;
using System.Net.Http;

namespace Pokedex.Common
{
    /// <summary>
    /// exception class to handle API/Third Party errors
    /// </summary>
    public class ThirdPartyAPIError : Exception
    {
        public readonly HttpStatusCode ErrorCode;

        public ThirdPartyAPIError(HttpStatusCode errorCode, string message)
            : base($"Error code: {errorCode} Message: {message}")
        {
            ErrorCode = errorCode;
        }

        public static ThirdPartyAPIError FromHttpResponse(HttpResponseMessage response) =>
            new(response.StatusCode, response.ReasonPhrase);
    }

}
