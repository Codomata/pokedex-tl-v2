﻿using Pokedex.Common.Interfaces;
using System;

namespace Pokedex.Common
{
    /// <summary>
    /// simple logger
    /// </summary>
    public class Logger : ILog
    {
        public void Log(Exception ex)
        {
            Console.WriteLine(ex.Message);
            Console.WriteLine(ex.StackTrace);
        }
    }
}