﻿using System;

namespace Pokedex.Common.Interfaces
{
    /// <summary>
    /// simple logging interface
    /// </summary>
    public interface ILog
    {
        public void Log(Exception ex);
    }
}