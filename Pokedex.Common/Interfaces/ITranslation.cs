﻿using System.Threading.Tasks;

namespace Pokedex.Common.Interfaces
{
    /// <summary>
    /// type of translation
    /// </summary>
    public enum TranslationType
    {
        Yoda,
        Shakespeare,
    }

    /// <summary>
    /// communication with translation api
    /// </summary>
    public interface ITranslation
    {
        /// <summary>
        /// translate the supplied string to relevant type
        /// </summary>
        /// <param name="input"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public Task<string> Translate(string input, TranslationType type);
    }
}
