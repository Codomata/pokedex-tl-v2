﻿using System;
using System.Threading.Tasks;

namespace Pokedex.Common.Interfaces
{
    /// <summary>
    /// Provides a wrapper on top of httpclient.
    /// </summary>
    public interface IHttpClient
    {
        /// <summary>
        /// executes specified endpoint and parses the json response to Type T 
        /// </summary>
        /// <param name="endpoint">endpoint to communicate with</param>
        /// <param name="cacheKey">if the responses are cached, cache key for the request</param>
        /// <typeparam name="T">success response type</typeparam>
        /// <returns></returns>
        public Task<T> Execute<T>(string endpoint, Func<string> cacheKey) where T : class;
    }
}