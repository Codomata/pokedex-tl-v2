﻿using Pokedex.Common.Model;
using System.Threading.Tasks;

namespace Pokedex.Common.Interfaces
{
    /// <summary>
    /// commonication with pokemon api
    /// </summary>
    public interface IPokeApi
    {
        /// <summary>
        /// get pokemon by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public Task<Pokemon> Get(string name);
    }
}
