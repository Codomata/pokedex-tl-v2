﻿using Newtonsoft.Json;
using Pokedex.Common.Interfaces;
using System;
using System.Net.Http;
using System.Runtime.Caching;
using System.Threading.Tasks;

namespace Pokedex.Common
{
    /// <summary>
    /// provides a custom httpclient wrapper which uses memory cache (multi threaded) to cache responses
    /// for requests for an absolute timeperiod
    /// </summary>
    public class HttpClientWithMemoryCache : IHttpClient
    {
        static readonly HttpClient client = new();
        readonly MemoryCache cache;
        readonly int cacheExpiryInSeconds;

        /// <summary>
        /// Constructor for HttpClient
        /// </summary>
        /// <param name="cache_name">unique name for the cache</param>
        /// <param name="cacheExpiryInSeconds">absolute expiry of cached response</param>
        public HttpClientWithMemoryCache(string cache_name, int cacheExpiryInSeconds)
        {
            this.cacheExpiryInSeconds = cacheExpiryInSeconds;
            this.cache = new MemoryCache(cache_name);
        }

        /// <summary>
        /// executes the endpoint first by attempting to retrieve from cache otherwise from httpclient
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="cacheKey"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public async Task<T> Execute<T>(string endpoint, Func<string> cacheKey) where T : class =>
            cache.Get(cacheKey()) as T ?? await GetFromAPI<T>(endpoint, cacheKey());

        /// <summary>
        /// retrieve via API client and add to cache
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="cacheKey"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        /// <exception cref="ThirdPartyAPIError"></exception>
        private async Task<T> GetFromAPI<T>(string endpoint, string cacheKey) where T : class
        {
            using var response = await client.GetAsync(endpoint);
            return response.IsSuccessStatusCode
                       ? AddToCache<T>(cacheKey, await response.Content.ReadAsStringAsync())
                       : throw ThirdPartyAPIError.FromHttpResponse(response);
        }

        /// <summary>
        /// adds item to cache
        /// </summary>
        /// <param name="cacheKey"></param>
        /// <param name="val"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        T AddToCache<T>(string cacheKey, string val)
        {
            var response = JsonConvert.DeserializeObject<T>(val);
            cache.Add(cacheKey, response, GetCacheItemPolicy());
            return response;
        }

        /// <summary>
        /// cache policy
        /// </summary>
        /// <returns></returns>
        CacheItemPolicy GetCacheItemPolicy() =>
            new()
            {
                AbsoluteExpiration = DateTimeOffset.Now.AddSeconds(cacheExpiryInSeconds)
            };
    }
}