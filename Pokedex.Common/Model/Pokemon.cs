﻿namespace Pokedex.Common.Model
{
    /// <summary>
    /// Pokemon 
    /// </summary>
    /// <param name="Name"></param>
    /// <param name="Description"></param>
    /// <param name="Habitat"></param>
    /// <param name="IsLegendary"></param>
    public record Pokemon(string Name, string Description, string Habitat, bool IsLegendary);
}
