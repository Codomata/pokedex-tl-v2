﻿using Pokedex.Common.Interfaces;
using Pokedex.Common.Model;
using System.Threading.Tasks;

namespace Pokedex.PokeAPI
{
    /// <summary>
    /// implements IPokeAPI for pokeapi.co
    /// </summary>
    public class PokeApiImpl : IPokeApi
    {
        public const int CacheExpiryInSeconds = 300;
        const string Endpoint = "https://pokeapi.co/api/v2/pokemon-species/";
        readonly IHttpClient client;

        public PokeApiImpl(IHttpClient client)
        {
            this.client = client;
        }

        public async Task<Pokemon> Get(string name) =>
            (await client.Execute<PKResponse>(Endpoint + name, () => name)).ToModel();
    }
}
