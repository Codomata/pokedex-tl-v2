﻿using Pokedex.Common.Model;
using System.Collections.Generic;
using System.Linq;

namespace Pokedex.PokeAPI
{
    internal record PKLanguage(string Name);
    internal record PKFlavorTextEntry(string Flavor_Text, PKLanguage Language);
    internal record PKHabitat(string Name);
    internal record PKResponse(string Name, PKHabitat Habitat, bool Is_Legendary, List<PKFlavorTextEntry> Flavor_Text_Entries)
    {
        internal Pokemon ToModel() =>
            new(
                Name: Name,
                Description:
                    Flavor_Text_Entries
                       ?.Where(f => f.Language.Name == "en")
                        .FirstOrDefault()
                       ?.Flavor_Text
                       ?.Replace("\n", " ")
                       ?.Replace("\f", " "),
                Habitat: Habitat?.Name,
                IsLegendary: Is_Legendary);
    }
}
