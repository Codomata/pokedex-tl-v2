using FunTranslations;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Pokedex.Common;
using Pokedex.Common.Interfaces;
using Pokedex.PokeAPI;

namespace Pokedex
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Pokedex", Version = "v1" });
            });

            services.AddSingleton(typeof(ILog), new Logger());

            services.AddSingleton(typeof(IPokeApi),
                                  new PokeApiImpl(
                                      new HttpClientWithMemoryCache(
                                          "pokeapi.co",
                                          PokeApiImpl.CacheExpiryInSeconds)));

            services.AddSingleton(typeof(ITranslation),
                                  new FunTranslationsImpl(
                                      new HttpClientWithMemoryCache(
                                          "Fun-Translations",
                                          FunTranslationsImpl.CacheExpiryInSeconds)));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Pokedex v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
