﻿using Microsoft.AspNetCore.Mvc;
using Pokedex.Common;
using Pokedex.Common.Interfaces;
using Pokedex.Common.Model;
using System;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Pokedex.Controllers
{
    [Route("pokemon/translated")]
    [ApiController]
    public class TranslationController : ControllerBase
    {
        readonly IPokeApi api;
        readonly ITranslation translator;
        readonly ILog log;

        public TranslationController(IPokeApi api, ITranslation translator, ILog log)
        {
            this.api = api;
            this.translator = translator;
            this.log = log;
        }


        // GET pokemon/translated/6
        [HttpGet("{name}")]
        public async Task<ActionResult<Pokemon>> Get(string name)
        {
            try
            {
                var pokeMon = await api.Get(name);
                pokeMon = pokeMon with
                {
                    Description = await GetDescription(
                                                translator,
                                                GetTranslationType(pokeMon),
                                                pokeMon.Description)
                };

                return pokeMon;
            }
            catch (Exception ex)
            {
                log.Log(ex);
                return Problem(detail: ex.Message, statusCode: ex is ThirdPartyAPIError error
                                                                   ? (int)error.ErrorCode
                                                                   : (int)HttpStatusCode.InternalServerError);
            }
        }

        static TranslationType GetTranslationType(Pokemon pokemon) =>
            pokemon.Habitat == "cave" || pokemon.IsLegendary
                ? TranslationType.Yoda
                : TranslationType.Shakespeare;

        static async Task<string> GetDescription(ITranslation translator, TranslationType type, string description)
        {
            try
            {
                return await translator.Translate(description, type);
            }
            catch
            {
                return description;
            }
        }
    }
}
