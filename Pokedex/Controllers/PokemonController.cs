﻿using Microsoft.AspNetCore.Mvc;
using Pokedex.Common;
using Pokedex.Common.Interfaces;
using Pokedex.Common.Model;
using System;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Pokedex.Controllers
{
    [Route("pokemon")]
    [ApiController]
    public class PokemonController : ControllerBase
    {
        readonly IPokeApi api;
        readonly ILog log;

        public PokemonController(IPokeApi api, ILog log)
        {
            this.api = api;
            this.log = log;
        }

        // GET pokemon/5
        [HttpGet("{name}")]
        public async Task<ActionResult<Pokemon>> Get(string name)
        {
            try
            {
                var pokeMon = await api.Get(name);
                return pokeMon;
            }
            catch (Exception ex)
            {
                log.Log(ex);
                return Problem(detail: ex.Message, statusCode: ex is ThirdPartyAPIError error
                                                                      ? (int)error.ErrorCode
                                                                      : (int)HttpStatusCode.InternalServerError);
            }
        }
    }
}
