# pokedex-tl

A simple REST API which return Pokemon details from `pokeapi.co` and provides fun translations using `funtranslations.com`

The solution is based in `C#` and is developed using `net core api` template in `Visual Studio 2019` / `Rider IDE`.
A `swagger` documentation is also integrated into the solution which can be used to test the API.

# Frameworks API etc.
- NET CORE 5
- Swagger
- Docker
- www.pokeapi.co
- www.funtranslations.com

# Build /Run

Requires net core sdk to be able to build and run this application. Only net core sdk required to be able to run. Please refer to microsoft resources for instructions on installing net core sdk in your respective environment.

## Build commands
Run following commands in the root of the application
- ```dotnet build```
- ```dotnet run```
## Docker
There is a docker file included in the solution and can be run using standard docker instructions (`https://docs.docker.com/samples/dotnetcore/`)

# Enhancements
- Currently I am using a cache in HTTP client communication with third party APIs. This is not practical for production environment and should be replaced with a more stable buffering router. The translation API is a subscription model and a fair usage policy should be applied.
- May need an authentication mechanism.
- 

