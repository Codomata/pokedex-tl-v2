using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using FunTranslations;
using Microsoft.AspNetCore.Mvc;
using Pokedex.Common;
using Pokedex.Common.Interfaces;
using Pokedex.Common.Model;
using Pokedex.Controllers;
using Pokedex.FunTranslations;
using Pokedex.PokeAPI;
using Xunit;

namespace Pokedex.UnitTests
{
    public class PokedexTest
    {
        static readonly Dictionary<string, (PKResponse, Pokemon)> dicPokemonData = new()
        {
            { "test1", CreateTuple("test1", "london", "Test 1 description", true)},
            { "test2", CreateTuple("test2", "Test 2 description", "milan", false)},
            { "test3", CreateTuple("test3", "Test 3 description", "cave", false)},
            { "test4", CreateTuple("test4", "Test 4 description", "cave", true)},
        };
        
        static (PKResponse, Pokemon) CreateTuple(string name, string description, string habitat, bool isLegendary) =>
            (new PKResponse(
                 name,
                 new PKHabitat(habitat),
                 isLegendary,
                 new List<PKFlavorTextEntry>()
                 {
                     {new PKFlavorTextEntry(description, new PKLanguage("en"))},
                     {new PKFlavorTextEntry(description + "random", new PKLanguage("random"))},
                 }),
             new Pokemon(name, description, habitat, isLegendary));
        
        [Theory]
        [InlineData("test1")]
        [InlineData("test2")]
        [InlineData("test3")]
        [InlineData("test4")]
        public void TestPokemonSuccess(string name)
        {
            var ctrl = new PokemonController(new PokeApiImpl(new PokeApiHttpMock()), new TestLog());
            var res  = ctrl.Get(name).Result;
            Assert.Equal(res.Value as Pokemon, dicPokemonData[name].Item2);
        }
        
        [Theory]
        [InlineData("test5")]
        [InlineData("test6")]
        public void TestPokemonNotFound(string name)
        {
            var ctrl = new PokemonController(new PokeApiImpl(new PokeApiHttpMock()), new TestLog());
            var res  = ctrl.Get(name).Result;
            Assert.True(res.Result is ObjectResult o && o.StatusCode == 404);
        }
        
        [Theory]
        [InlineData("test1", "yoda")]
        [InlineData("test2", "shakespeare")]
        [InlineData("test3", "yoda")]
        [InlineData("test4", "yoda")]
        public void TestTranslation(string name, string expectedTranslation)
        {
            var ctrl = new TranslationController(new PokeApiImpl(new PokeApiHttpMock()),
                                                 new FunTranslationsImpl(new FunTranslationHttpMock()), 
                                                 new TestLog());
            
            var res  = ctrl.Get(name).Result.Value as Pokemon;
            Assert.Equal(res.Description, dicPokemonData[name].Item2.Description + $"${expectedTranslation}");
        }

        class PokeApiHttpMock : IHttpClient
        {
            public Task<T> Execute<T>(string endpoint, Func<string> cacheKey) where T : class =>
                Task.Run(() => dicPokemonData.TryGetValue(NameFromEndpoint(endpoint), out var res)
                                   ? res.Item1 as T
                                   : throw new ThirdPartyAPIError(HttpStatusCode.NotFound, "Not found"));

            static string NameFromEndpoint(string endpoint)
            {
                var split = endpoint.Split("/");
                return split[^1];
            }

        }
        
        class FunTranslationHttpMock : IHttpClient
        {
            public Task<T> Execute<T>(string endpoint, Func<string> cacheKey) where T : class =>
                Task.Run(() => Translate(endpoint) as T);

            static FTResponse Translate(string endpoint)
            {
                var split = endpoint.Split("/");
                var text  = split[^1][(split[^1].IndexOf("=") + 1)..];
                var type  = split[^1].Split(".")[0];

                return new FTResponse(
                    new FTSuccess(1),
                    new FTContents((text + "$" + type), text, type));
            }

        }

        class TestLog : ILog
        {
            public List<Exception> Errors = new();

            public void Log(Exception ex) =>
                Errors.Add(ex);
        }
        
    }
}
