﻿using Pokedex.Common.Interfaces;
using Pokedex.FunTranslations;
using System;
using System.Threading.Tasks;

namespace FunTranslations
{
    /// <summary>
    /// implements ITranslation for funtranslation.com
    /// </summary>
    public class FunTranslationsImpl : ITranslation
    {
        public const int CacheExpiryInSeconds = 300;
        const string YodaEndpoint = "https://api.funtranslations.com/translate/yoda.json?text=";
        const string ShakespeareEndpoint = "https://api.funtranslations.com/translate/shakespeare.json?text=";

        readonly IHttpClient client;

        public FunTranslationsImpl(IHttpClient client) =>
            this.client = client;

        public async Task<string> Translate(string input, TranslationType type) =>
            (await client.Execute<FTResponse>(GetEndpoint(type) + input, () => CacheKey(input, type))).Contents.Translated;

        static string GetEndpoint(TranslationType type) =>
            type switch
            {
                TranslationType.Shakespeare => ShakespeareEndpoint,
                TranslationType.Yoda => YodaEndpoint,
                _ => throw new InvalidOperationException($"Translation type {type} not implemented")
            };

        static string CacheKey(string input, TranslationType type) =>
            $"{type}:{input}";

    }

}
