﻿namespace Pokedex.FunTranslations
{
    internal record FTSuccess(int Total);

    internal record FTContents(string Translated, string Text, string Translation);

    internal record FTResponse(FTSuccess Success, FTContents Contents);
}
